(function($){  
  Drupal.behaviors.doubleClickNA = {
    attach: function (context, settings) {
      // Hide the N/A radio button
      $('.form-radio[value=_none]').parent().hide();
  
      // Enable the double-click unchecker function
      $('.form-radio', context).dblclick(function(event) {
        $(this).prop('checked', false);
        $(this).parent().parent().$('.form-radio[value=_none]').prop('checked', true);
        $(this).after('<div>Well, I guess it sorta works...</div>');
      });
    }
  };
})(jQuery);
